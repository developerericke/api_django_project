
from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('get_keys/',views.dashboard_show_keys),#getAvailableApiKeys),
    path('generate_key/',views.registerNewKey),
    path('delete_keys/',views.delete_key),
    path('api/post/',views.fetch_query),
    #path('api/sqlescape/',views.expose_escape_funtion),
    path('dashboard/',views.getAPIpApiPage),
    path('test/',views.test_a),
    path('categories/',views.get_categories),
    path('units/',views.get_units),
    path('manufacturers/',views.get_manufacturers),
    path('subcategories/',views.get_subcategories),
    path('suppliers/',views.get_suppliers),
    path('products/get/price/',views.get_products_price),
    path('items/add',views.new_item),
    path('products/add',views.add_product),
    path('prices/add',views.new_price),
    path('images/add',views.new_image),
    path('manufacturers/add',views.new_manufacturer),
    path('supplier/add',views.new_supplier),
    path('sql/api/',include('apis.urls')),
    path('accounts/',include('accounts.urls')),
]
