from django.http import HttpResponse,HttpResponseServerError,JsonResponse
from django.shortcuts import render
from datetime import  datetime,time
import secrets
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_exempt
import re
import pymysql.cursors
from .db_credentials import host,password,user,database
import ast
import mysql.connector
import mysql.connector.errors
import mysql.connector.pooling
import mysql.connector.connection
from django.db import connection
from .sql_escape import escape_val 



def  fetch_records(sql):
    resp = {"status":200,"records":[],"message":"ok"}
    try:
     cursor = connection.cursor()
     cursor.execute(sql)
     result = cursor.fetchall()
     cursor.close()
     resp = {"status":200,"records":result,"message":"ok"}
        
    except Exception as e:
        resp = {"status":500,"records":[],"message":str(e)}
    
    
    return resp


def modify_record(sql):
    resp = {"status":200,"rowsaffected":0,"message":"ok"}
    try:
        cursor = connection.cursor()
        cursor.execute(sql)
        connection.commit()
        resp = {"status":200,"rowsaffected":cursor.rowcount,"message":"ok"}
    except Exception as e:
        resp = {"status":500,"rowsaffected":0,"message":str(e)}

    return resp


def validate_api(user,key):
  
    escaped_key = escape_val(key)
    escaped_user = escape_val(user)
    sql = "SELECT * from daudtco_BD_Prices.T_api where api_user = {} and api_key ={}".format(escaped_user,escaped_key)

    
    result = fetch_records(sql)
    permission_flags = {"read":False,"insert":False,"update":False,"delete":False}

    if key is not None and user is not None and result['status'] == 200 and len(result['records']) ==1:
        
        #permission_flags = {"read":True,"insert":True,"update":True,"delete":True}
        permission_flags['read'] = True
        if result['records'][0][4] == 1:
           permission_flags['insert'] = True
        if result['records'][0][5] == 1:
            permission_flags['update'] = True
        if result['records'][0][6] == 1:
            permission_flags['delete'] = True

        return permission_flags
        
    else:
        return permission_flags

@csrf_exempt
def get_categories(request):
      type = request.GET.get('id')
      user = request.GET.get('user')
      key = request.GET.get('key')

    
      
      def get_subcategory_by_id(id):
          sql = "SELECT * from daudtco_BD_Prices.T_Subcategory where ID_CAT = {} ".format(id)
          records = fetch_records(sql)
          
          if records['status'] == 200:
              return records['records']
          else:
              return []    


      def get_all_categories():
          categories = []
          sql = "SELECT * from daudtco_BD_Prices.T_Category"
          records = fetch_records(sql)
          
          if records['status'] == 200:
              for row in records['records']:
                  associated_sub_categories  = []
                  for sub_cat in get_subcategory_by_id(row[0]):
                      associated_sub_categories.append({"ID":sub_cat[0],"SCat":sub_cat[1],"Sub_Uuid":sub_cat[3],"Sub_Link":sub_cat[4],"Sub_Corel":sub_cat[5]})
                  categories.append({"ID":row[0],"Cat":row[1],"SubCat":associated_sub_categories})

              return categories    

          else:
              err_val = {"Error":records["message"]}
              return err_val

      def get_specific_category(id):
          categories = []
          sql = "SELECT * from daudtco_BD_Prices.T_Category where ID_Cat = {}".format(id)
          records = fetch_records(sql)
          
          if records['status'] == 200:
              for row in records['records']:
                  associated_sub_categories  = []
                  for sub_cat in get_subcategory_by_id(row[0]):
                      associated_sub_categories.append({"ID":sub_cat[0],"SCat":sub_cat[1],"Sub_Uuid":sub_cat[3],"Sub_Link":sub_cat[4],"Sub_Corel":sub_cat[5]})
                  categories.append({"ID":row[0],"Cat":row[1],"SubCat":associated_sub_categories})

              return categories    

          else:
              return {"Error":records["message"]}

      if  validate_api(user,key)['read']==True :
          categories_data = []
          if type is not None and type == "all":
            categories_data = get_all_categories()
            return JsonResponse(status=200 ,data={"VAL":categories_data})
          elif type is not None :
            try:
                int(type)
                categories_data = get_specific_category(type)
                return JsonResponse(status=200 ,data={"VAL":categories_data})
            except:
                return JsonResponse(status=400 ,data={"Error":"Invalid Category ID"})    
          else:
            categories_data = get_all_categories()
            return JsonResponse(status=200 ,data={"VAL":categories_data})

      else:
        return HttpResponse(status=401,content="Permissions Denied")    

@csrf_exempt
def get_units(request):
      
    type = request.GET.get('id')
    user = request.GET.get('user')
    key = request.GET.get('key') 


    def get_specific_unit(id):
        units = []
        sql = "SELECT * from daudtco_BD_Prices.T_Units where ID_Unit = {}".format(id)
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                units.append({"ID":row[0],"Unit":row[1]})
            return units
        else:
            return {"error":records['message']}  
    
    def get_all_units():
        units = []
        sql = "SELECT * from daudtco_BD_Prices.T_Units"
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                units.append({"ID":row[0],"Unit":row[1]})
            return units
        else:
            return {"error":records['message']}    

    if  validate_api(user,key)['read'] is True :
        categories_data = []

        if type is not None and type != "all" :
          try:
            int(type)
            categories_data = get_specific_unit(type)
            return JsonResponse(status=200 ,data={"VAL1":categories_data})
          except:
            return JsonResponse(status=400 ,data={"Error":"Invalid Unit ID"})    
        else:
          categories_data = get_all_units()
          return JsonResponse(status=200 ,data={"VAL1":categories_data})

    else:
      return HttpResponse(status=401,content="Permissions Denied")         


@csrf_exempt
def get_manufacturers(request):
      
    type = request.GET.get('id')
    user = request.GET.get('user')
    key = request.GET.get('key') 


    def get_specific_manufacturer(id):
        manufacturers = []
        sql = "SELECT * from daudtco_BD_Prices.T_Manufacturer where ID_Man = {}".format(id)
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                manufacturers.append({"ID":row[0],"Man":row[1],"Link":row[2]})
            return manufacturers
        else:
            return {"error":records['message']}  
    
    def get_all_manufacturers():
        manufacturers = []
        sql = "SELECT * from daudtco_BD_Prices.T_Manufacturer"
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                manufacturers.append({"ID":row[0],"Man":row[1],"Link":row[2]})
            return manufacturers
        else:
            return {"error":records['message']}    

    if  validate_api(user,key)['read'] is True :
        manufacturers_data = []

        if type is not None and type != "all" :
          try:
            int(type)
            manufacturers_data = get_specific_manufacturer(type)
            return JsonResponse(status=200 ,data={"Manufacturers":manufacturers_data})
          except:
            return JsonResponse(status=400 ,data={"Error":"Invalid Manufacturer ID"})    
        else:
          manufacturers_data = get_all_manufacturers()
          return JsonResponse(status=200 ,data={"Manufacturers":manufacturers_data})

    else:
      return HttpResponse(status=401,content="Permissions Denied")         


@csrf_exempt
def get_suppliers(request):
      
    type = request.GET.get('id')
    user = request.GET.get('user')
    key = request.GET.get('key') 


    def get_specific_supplier(id):
        manufacturers = []
        sql = "SELECT * from daudtco_BD_Prices.T_Supplier where ID_Supp = {}".format(id)
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                manufacturers.append({"ID":row[0],"Name":row[1],"Link":row[2]})
            return manufacturers
        else:
            return {"error":records['message']}  
    
    def get_all_suppliers():
        suppliers = []
        sql = "SELECT * from daudtco_BD_Prices.T_Supplier"
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                if row[12] == 1: #supplier is private
                      suppliers.append({"ID":row[0],"Name":'fornecedor genérico',"Link":row[2],"private":row[12]})
                else:
                      suppliers.append({"ID":row[0],"Name":row[1],"Link":row[2],"private":row[12]})
                
            return suppliers
        else:
            return {"error":records['message']}    

    if  validate_api(user,key)['read'] is True :
        suppliers_data = []

        if type is not None and type != "all" :
          try:
            int(type)
            suppliers_data = get_specific_supplier(type)
            return JsonResponse(status=200 ,data={"Suppliers":suppliers_data})
          except:
            return JsonResponse(status=400 ,data={"Error":"Invalid Supplier ID"})    
        else:
          suppliers_data = get_all_suppliers()
          return JsonResponse(status=200 ,data={"Suppliers":suppliers_data})

    else:
      return HttpResponse(status=401,content="Permissions Denied")         


@csrf_exempt
def get_subcategories(request):
      
    type = request.GET.get('id')
    user = request.GET.get('user')
    key = request.GET.get('key') 


    def get_specific_subcategory(id):
        subcategories = []
        sql = "SELECT * from daudtco_BD_Prices.T_Subcategory where ID_Sub = {}".format(id)
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                subcategories.append({"ID":row[0],"Name":row[1]})
            return subcategories
        else:
            return {"error":records['message']}  
    
    def get_all_suppliers():
        subcategories = []
        sql = "SELECT * from daudtco_BD_Prices.T_Subcategory"
        records = fetch_records(sql)
      
        if records['status'] == 200:
            for row in records['records']:
                subcategories.append({"ID":row[0],"Name":row[1]})
            return subcategories
        else:
            return {"error":records['message']}    

    if  validate_api(user,key)['read'] is True :
        subcategories_data = []

        if type is not None and type != "all" :
          try:
            int(type)
            subcategories_data = get_specific_subcategory(type)
            return JsonResponse(status=200 ,data={"Subcategories":subcategories_data})
          except:
            return JsonResponse(status=400 ,data={"Error":"Invalid Supplier ID"})    
        else:
          subcategories_data = get_all_suppliers()
          return JsonResponse(status=200 ,data={"Subcategories":subcategories_data })

    else:
      return HttpResponse(status=401,content="Permissions Denied")         


def dashboard_show_keys(request):
  
    if request.user.is_authenticated:
        sql = "SELECT * from daudtco_BD_Prices.T_api"
        records = fetch_records(sql)
        print(records)

        if records['status'] == 200:
            prepared_keys = []
            for row in records['records']:
                
                  prepared_keys.append({"api_user":row[0],"api_key":row[1],"date_created":row[2],"can_read":row[3],"can_insert":row[4],"can_update":row[5],"can_delete":row[6]})                
            return JsonResponse(status=200 ,data={"keys":prepared_keys,"tables":[]})
        else:
            return JsonResponse(status=400 ,data={"Error":records['message']})    
        
    else:
        return JsonResponse(status=401 ,data={"Error":"Permission Denied"})    

@csrf_exempt
def delete_key(request):

  if request.user.is_authenticated:
    key = request.POST.get('key')

    if key is not None:
        
        sql = "DELETE from daudtco_BD_Prices.T_api where api_key = '{}'".format(key)
        deletetCount = modify_record(sql)
        if deletetCount['status'] == 200:
            if deletetCount['rowsaffected'] == 1:
              return JsonResponse({"state":200})
            else:
             return JsonResponse(status=500,data={"error":"Failed to Delete"})
        else:
            return JsonResponse(status=500,data={"Error":deletetCount['message']})
    else:
        return JsonResponse(status=400,data={"Error":"Missing Key"})    

  else:
      return JsonResponse(status=401 ,data={"Error":"Permission Denied"})   


@csrf_exempt
def registerNewKey(request):
  if request.user.is_authenticated: 
    key_user = request.POST.get('apiuser_new')
    key_perm_insert = request.POST.get('api_priv_create')
    #key_perm_read = request.POST.get('api_priv_read')
    key_perm_update = request.POST.get('api_priv_update')
    key_perm_delete = request.POST.get('api_priv_delete')
    
    if key_user is not None:
        keyGen = secrets.token_urlsafe(12) 
        tdDate = str(datetime.today().strftime('%Y-%m-%d') )
        insert_flag = 0
        update_flag = 0
        delete_flag =0
        if key_perm_insert is not None:
            try:
                int(key_perm_insert)
                insert_flag = 1
            except:
                insert_flag = 0
        
        if key_perm_update is not None:
            try:
                int(key_perm_update)
                update_flag = 1
            except:
                update_flag = 0
        
        if key_perm_delete is not None:
            try:
                int(key_perm_delete)
                delete_flag = 1
            except:
                delete_flag = 0
        
        #if insert_flag == 1 and update_flag == 1 and delete_flag == 1:
        sql = "INSERT INTO daudtco_BD_Prices.T_api (api_user,api_key,date_created,can_read,can_insert,can_update,can_delete) VALUES ('{}','{}','{}',1,{},{},{})".format(key_user,keyGen,tdDate,insert_flag,update_flag,delete_flag)
        insert_key = modify_record(sql)
        if insert_key['status'] == 200:
           if insert_key['rowsaffected'] == 1:
                  return JsonResponse({"state":200,"key":keyGen})
           else:
                return JsonResponse(status=500,data={"error":"Failed to Insert"})
        else:
                return JsonResponse(status=500,data={"Error":insert_key['message']})
    #else:
     #       return JsonResponse(status=400,data={"Error":"Invalid Permissions"})
    else:
        return JsonResponse(status=400,data={"Error":"Missing Key"})    

  else:
      return JsonResponse(status=401 ,data={"Error":"Permission Denied"})   



def test_a(request):
    user = request.GET.get('user')
    key = request.GET.get('key')

    if  validate_api(user,key):
        return HttpResponse(status=200,content="ok")
    else:
        return HttpResponse(status=401,content="Permissions Denied")    

def validate_field(field,is_number,message):
       if field is not None: 
        if is_number == True:
         try:
            int(field)
            
            return True
         except:
            return "Must be a number : "+ str(message)
        else:
            return True 
       else:
           return str(message)

#inserts
@csrf_exempt
def new_item(request):
    items = request.POST.get('items')
    user = request.POST.get('user')
    key = request.POST.get('key')
    
    
    # def validate_field(field,is_number,message):
    #    if field is not None: 
    #     if is_number == True:
    #      try:
    #         int(field)
            
    #         return True
    #      except:
    #         return "Must be a number : "+ str(message)
    #     else:
    #         return True 
    #    else:
    #        return str(message)


    try:
     if  validate_api(user,key)['insert'] == True:
        #Fields
        #ID_Itm,Itm_Code,ID_Sup-req,Itm_Status-req,ID_Man ,Bot_Link-req ,Itm_Desc-req ,Itm_Det-req ,Itm_user-req,ID_Unit,ID_Sub,
        if items is None:
            return  JsonResponse(status=400,data={"error":"Items to insert required"})
        else:
                items_to_insert = []
                failed_inserts = []
                success_inserts = []
                try:
                    items_to_insert = ast.literal_eval(items)
                except:
                    do_nothing = True    

                for to_insert in items_to_insert:    
                
                    validations_state = []
                    
                    M_supplier_id = None
                    try:
                        M_supplier_id = to_insert['M_supplier_id']
                    except:
                        M_supplier_id= None    
                   
                    M_item_status = None
                    try:
                        M_item_status = to_insert['M_item_status']
                    except:
                        M_item_status = None    
                    
                    M_bot_link = None
                    try:
                        M_bot_link = to_insert['M_bot_link']
                    except:
                            M_bot_link = None

                    
                    M_item_desc = None
                    try:
                        M_item_desc = to_insert['M_item_desc']
                    except:
                        M_item_desc = None     


                    M_item_det = None
                    try:
                        M_item_det = to_insert['M_item_det']
                    except:
                        M_item_det = None

                     
                    M_item_user = None
                    try:
                        M_item_user = to_insert['M_item_user']
                    except:
                        M_item_user = None     

                    #Validate that each required field is available
                    validations_state.append(validate_field(field=M_supplier_id,is_number=True,message="M_supplier_id is Required"))
                    validations_state.append(validate_field(field=M_item_status ,is_number=True,message="M_item_status is Required"))
                    validations_state.append(validate_field(field=M_bot_link ,is_number=False,message="M_bot_link is Required"))
                    validations_state.append(validate_field(field=M_item_desc ,is_number=False,message="M_item_desc is Required"))
                    validations_state.append(validate_field(field=M_item_det ,is_number=False,message="M_item_det is Required"))
                    validations_state.append(validate_field(field=M_item_user ,is_number=True,message="M_item_user is Required"))
                    
                    failed_validations = []

                    #Validate that each required field is available

                    for validation in validations_state:
                        if validation != True:
                          
                            failed_validations.append(validation)

                    non_required_sql_fields = ''
                    non_required_sql_values = ''
                    
                    #Verify Data types if Non-mandatry fields have been provided
                    try:
                     if to_insert['O_item_code'] is not None:
                        itm_code =  escape_val(to_insert['O_item_code'])
                        non_required_sql_fields = non_required_sql_fields + ","+ "Itm_Code" + ','
                        non_required_sql_values = non_required_sql_values + ","+ itm_code +','
                    except:
                        do_nothing = True
                    try:    
                     if to_insert['O_id_man'] is not None:
                      try:
                        int(to_insert['O_id_man'])    
                        ID_Man = to_insert['O_id_man']
                        non_required_sql_fields = non_required_sql_fields + "," + "ID_Man" + ','
                        non_required_sql_values = non_required_sql_values + "," + ID_Man+','
                      except:
                        failed_validations.append("O_id_man must be a Number")  
                    except:
                        do_nothing = True
                    try:    
                     if to_insert['O_id_unit'] is not None:
                      try:
                        int(to_insert['O_id_unit'])    
                        ID_Unit = int(to_insert['O_id_unit'])
                        non_required_sql_fields = non_required_sql_fields + "," + "ID_Unit" + ','
                        non_required_sql_values = non_required_sql_values + ","+str(ID_Unit)+','
                      except Exception as e:
                         
                        failed_validations.append("O_id_unit must be a number")    
                    except:
                        do_nothing = True

                    try:
                     if to_insert['O_id_sub'] is not None:
                        try:
                            int(to_insert['O_id_sub'])
                            ID_Sub = to_insert['O_id_sub']
                            non_required_sql_fields = non_required_sql_fields + "," + "ID_Sub"
                            non_required_sql_values = non_required_sql_values + "," + ID_Sub     
                        except:
                            failed_validations.append("O_id_sub must be a number")
                    except:
                        do_nothing = True
                    if len(failed_validations) > 0:
                        failed_msg = ''
                        for fl in failed_validations:
                            failed_msg = failed_msg + str(fl) + ' , '
                        failed_inserts.append({"Item":to_insert,"Error":"Some errors were encountered - " + failed_msg})
                    else:
                        mandatory_sql_fields ='ID_Itm,ID_Sup,Itm_Status,Bot_Link,Itm_Desc,Itm_Det,Itm_user,'
                        mandatory_sql_values ='NULL,'+str(M_supplier_id)+','+str(M_item_status)+','+escape_val(M_bot_link)+','+escape_val(M_item_desc)+','+escape_val(M_item_det)+','+str(M_item_user)+','                 
                        
                        constructed_sql = "INSERT INTO daudtco_BD_Prices.T_Item_Bot ("+mandatory_sql_fields+non_required_sql_fields+") VALUES ("+mandatory_sql_values+non_required_sql_values+");"
                        constructed_sql= constructed_sql.replace(',)',')')
                        constructed_sql= constructed_sql.replace(',,',',')
                      
                        
                        insert_state = modify_record(constructed_sql)
                        insert_state_code = 200
                        if insert_state['status']==200:
                            if insert_state['rowsaffected'] == 1:
                             success_inserts.append({"Item":to_insert,"Success":"Inserted Successfully"})
                        else:
                            insert_state_code = 400
                            failed_inserts.append({"Item":to_insert,"Error":"Failed to Insert - " + insert_state['message']})               
                return JsonResponse(status=200,data={"success_inserted_count":len(success_inserts),"failed_inserts_count":len(failed_inserts),"success_items":success_inserts,"failed_items":failed_inserts})
     else:
        
        return JsonResponse(status=401,data={"Error":"Permissions Denied"})    
    except:
        return JsonResponse(status=500,data={"Error":"Internal Server Error"})

@csrf_exempt
def new_price(request):
    items = request.POST.get('items')
    user = request.POST.get('user')
    key = request.POST.get('key')
    try:
     if  validate_api(user,key)['insert'] == True:
         if items is None:
                return  JsonResponse(status=400,data={"error":"Price(s) to insert required"})
         else:
            #Mandatory Fields
            #ID_Itm,Pric_Value,Pric_Date,Pric_user,Pric_Duedate,Pric_obsolete
                items_to_insert = []
                failed_inserts = []
                success_inserts = []
                try:
                    items_to_insert = ast.literal_eval(items)
                except:
                    do_nothing = True    

             
                for to_insert in items_to_insert:    
                    M_item_id = None
                    try:
                        M_item_id = to_insert['M_item_id']
                    except:
                        M_item_id = None

                    M_price_value = None
                    try:
                        M_price_value = to_insert['M_price_value']
                    except:
                        M_price_value = None    

                    M_price_user = None
                    try:
                        M_price_user = to_insert['M_price_user']
                    except:
                        M_price_user = None        

                
                    validations_state = []
                    #Validate that each required field is available
                    validations_state.append(validate_field(field=M_item_id,is_number=True,message="M_item_id is Required"))
                    validations_state.append(validate_field(field=M_price_value ,is_number=False,message="M_price_value is Required and must be a decimal"))
                    validations_state.append(validate_field(field=M_price_user ,is_number=False,message="M_price_user is Required"))
                  
                    failed_validations = []

                    #Validate that each required field is available

                    for validation in validations_state:
                        if validation != True:
                          
                            failed_validations.append(validation)

                    non_required_sql_fields = ''
                    non_required_sql_values = ''
                    
                    #Verify Data types if Non-mandatry fields have been provided
                    try:
                     if to_insert['O_price_date'] is not None:
                        Pric_Date = str(to_insert['O_price_date'])
                        non_required_sql_fields = non_required_sql_fields + ","+ "Pric_Date" + ','
                        non_required_sql_values = non_required_sql_values + ","+ escape_val(str(Pric_Date)) +','
                    except:
                        do_nothing = True
                    try:    
                     if to_insert['O_price_due_date'] is not None:
                      #try:
                        #int(to_insert['O_price_due_date'])    
                        Pric_Duedate = str(to_insert['O_price_due_date'])
                        non_required_sql_fields = non_required_sql_fields + "," + "Pric_Duedate" + ','
                        non_required_sql_values = non_required_sql_values + "," + escape_val(str(Pric_Duedate))+','
   
                    except:
                        do_nothing = True
                    try:    
                     if to_insert['O_price_obsolete'] is not None:
                     # try:
                        #int(to_insert['ID_Unit'])    
                        Pric_obsolete = str(to_insert['O_price_obsolete'])
                        non_required_sql_fields = non_required_sql_fields + "," + "Pric_obsolete" + ','
                        non_required_sql_values = non_required_sql_values + ","+ escape_val(str(Pric_obsolete))+','
                      #except Exception as e:
                         
                       # failed_validations.append("ID_Unit must be a number")    
                    except Exception as e:
                       
                        do_nothing = True

               
                    if len(failed_validations) > 0:
                        failed_msg = ''
                        for fl in failed_validations:
                            failed_msg = failed_msg + str(fl) + ' , '
                        failed_inserts.append({"Price":to_insert,"Error":"Some errors were encountered - " + failed_msg})
                    else:
                        mandatory_sql_fields ='ID_Pric,ID_Itm,Pric_Value,Pric_user,'
                        mandatory_sql_values ='NULL,'+str(to_insert['M_item_id'])+','+str(to_insert['M_price_value'])+','+escape_val(to_insert['M_price_user'])+','                 
                        
                        constructed_sql = "INSERT INTO daudtco_BD_Prices.T_Prices ("+mandatory_sql_fields+non_required_sql_fields+") VALUES ("+mandatory_sql_values+non_required_sql_values+");"
                        constructed_sql= constructed_sql.replace(',)',')')
                        constructed_sql= constructed_sql.replace(',,',',')
                        
                   
                        insert_state = modify_record(constructed_sql)
                        insert_state_code = 400
                        if insert_state['status']==200:
                            if insert_state['rowsaffected'] == 1:
                              success_inserts.append({"Price":to_insert,"Success":"Inserted Successfully"})
                              insert_state_code = 200
                        else:
                              insert_state_code = 400
                              failed_inserts.append({"Price":to_insert,"Error":"Failed to Insert - " + insert_state['message']})               
                return JsonResponse(status=200,data={"success_inserted_count":len(success_inserts),"failed_inserts_count":len(failed_inserts),"success_items":success_inserts,"failed_items":failed_inserts})

            

            
     else:
           return JsonResponse(status=401,data={"Error":"Permissions Denied"})     
    except:
        return JsonResponse(status=500,data={"Error":"Internal Server Error"})

@csrf_exempt 



@csrf_exempt
def new_image(request):
    items = request.POST.get('items')
    user = request.POST.get('user')
    key = request.POST.get('key')
    try:
     if  validate_api(user,key)['insert'] == True:
         if items is None:
                return  JsonResponse(status=400,data={"error":"Image(s) to insert required"})
         else:
            #Mandatory Fields
            #ID_Image,ID_Itm,Img_Link
                items_to_insert = []
                failed_inserts = []
                success_inserts = []
                try:
                    items_to_insert = ast.literal_eval(items)
                except:
                    do_nothing = True    

             
                for to_insert in items_to_insert:    
                
                    validations_state = []
                    M_item_id = None
                    try:
                        M_item_id = to_insert['M_item_id']
                    except:
                        M_item_id = None

                    M_image_link = None

                    try:
                        M_image_link = to_insert['M_image_link']
                    except:
                        M_image_link = None    

                    #Validate that each required field is available
                    validations_state.append(validate_field(field=M_item_id,is_number=True,message="M_item_id is Required"))
                    validations_state.append(validate_field(field=M_image_link ,is_number=False,message="M_image_link is Required"))
                   
                    failed_validations = []

                    #Validate that each required field is available

                    for validation in validations_state:
                        if validation != True:
                          
                            failed_validations.append(validation)

                    non_required_sql_fields = ''
                    non_required_sql_values = ''
                    
                  
                  
                   
               
                    if len(failed_validations) > 0:
                        failed_msg = ''
                        for fl in failed_validations:
                            failed_msg = failed_msg + str(fl) + ' , '
                        failed_inserts.append({"Image":to_insert,"Error":"Some errors were encountered - " + failed_msg})
                    else:
                        mandatory_sql_fields ='ID_Image,ID_Itm,Img_Link'
                        mandatory_sql_values ='NULL,'+str(to_insert['M_item_id'])+','+escape_val(to_insert['M_image_link'])                
                        
                        constructed_sql = "INSERT INTO daudtco_BD_Prices.T_Images ("+mandatory_sql_fields+") VALUES ("+mandatory_sql_values+");"
                        constructed_sql= constructed_sql.replace(',)',')')
                        constructed_sql= constructed_sql.replace(',,',',')
                        
                        #print(constructed_sql) 
                        insert_state = modify_record(constructed_sql)
                        #insert_state_code = 400
                        if insert_state['status']==200:
                             if insert_state['rowsaffected'] == 1:
                               success_inserts.append({"Image":to_insert,"Success":"Inserted Successfully"})
                               #insert_state_code = 200
                        else:
                               #insert_state_code = 400
                               failed_inserts.append({"Image":to_insert,"Error":"Failed to Insert - " + insert_state['message']})               
                
                return JsonResponse(status=200,data={"success_inserted_count":len(success_inserts),"failed_inserts_count":len(failed_inserts),"success_items":success_inserts,"failed_items":failed_inserts})

            

            
     else:
           return JsonResponse(status=401,data={"Error":"Permissions Denied"})     
    except:
        return JsonResponse(status=500,data={"Error":"Internal Server Error"})     


@csrf_exempt 

@csrf_exempt
def new_manufacturer(request):
    items = request.POST.get('items')
    user = request.POST.get('user')
    key = request.POST.get('key')
    try:
     if  validate_api(user,key)['insert'] == True:
         if items is None:
                return  JsonResponse(status=400,data={"error":"Manufacturer(s) to insert required"})
         else:
            #Mandatory Fields
            #ID_Itm,Pric_Value,Pric_Date,Pric_user,Pric_Duedate,Pric_obsolete
                items_to_insert = []
                failed_inserts = []
                success_inserts = []
                
                try:
                    items_to_insert = ast.literal_eval(items)
                except:
                    do_nothing = True    
                insert_state_code = 400
             
                for to_insert in items_to_insert:    
                    M_manufacturer_name = None
                    try:
                     M_manufacturer_name=  to_insert['M_manufacturer_name']
                    except:
                        M_manufacturer_name= None

                    
                    M_manufacturer_link = None
                    try:
                        M_manufacturer_link = to_insert['M_manufacturer_link']
                    except:
                        M_manufacturer_link=None    
                
                    validations_state = []
                    #Validate that each required field is available
                    validations_state.append(validate_field(field=M_manufacturer_name ,is_number=False,message="M_manufacturer_name is Required !"))
                    validations_state.append(validate_field(field=M_manufacturer_link ,is_number=False,message="M_manufacturer_link is Required !"))
                  
                    failed_validations = []

                    #Validate that each required field is available

                    for validation in validations_state:
                        if validation != True:
                          
                            failed_validations.append(validation)

                    non_required_sql_fields = ''
                    non_required_sql_values = ''
                   
               
                    if len(failed_validations) > 0:
                        failed_msg = ''
                        for fl in failed_validations:
                            failed_msg = failed_msg + str(fl) + ' , '
                        failed_inserts.append({"Manufacturer":to_insert,"Error":"Some errors were encountered - " + failed_msg})
                    else:
                        mandatory_sql_fields ='ID_Man,Man_Name,Man_Link'
                        mandatory_sql_values ='NULL,'+escape_val(M_manufacturer_name)+','+escape_val(M_manufacturer_link)                 
                        
                        constructed_sql = "INSERT INTO daudtco_BD_Prices.T_Manufacturer ("+mandatory_sql_fields+") VALUES ("+mandatory_sql_values+");"
                        constructed_sql= constructed_sql.replace(',)',')')
                        constructed_sql= constructed_sql.replace(',,',',')
                        
                   
                        insert_state = modify_record(constructed_sql)
                        insert_state_code = 400
                        if insert_state['status']==200:
                            if insert_state['rowsaffected'] == 1:
                              success_inserts.append({"Manufacturer":to_insert,"Success":"Inserted Successfully"})
                              insert_state_code = 200
                        else:
                              insert_state_code = 400
                              failed_inserts.append({"Manufacturer":to_insert,"Error":"Failed to Insert - " + insert_state['message']})               
                
                return JsonResponse(status=200,data={"success_inserted_count":len(success_inserts),"failed_inserts_count":len(failed_inserts),"success_items":success_inserts,"failed_items":failed_inserts})

            

            
     else:
           return JsonResponse(status=401,data={"Error":"Permissions Denied"})     
    except:
        return JsonResponse(status=500,data={"Error":"Internal Server Error"}) 

@csrf_exempt
def new_supplier(request):
    items = request.POST.get('items')
    user = request.POST.get('user')
    key = request.POST.get('key')
    
    try:
     if  validate_api(user,key)['insert'] == True:
        #Fields
        #ID_Itm,Itm_Code,ID_Sup-req,Itm_Status-req,ID_Man ,Bot_Link-req ,Itm_Desc-req ,Itm_Det-req ,Itm_user-req,ID_Unit,ID_Sub,
        if items is None:
            return  JsonResponse(status=400,data={"error":"Supplier(s) to insert required"})
        else:
                insert_state_code = 400
                items_to_insert = []
                failed_inserts = []
                success_inserts = []
                try:
                    items_to_insert = ast.literal_eval(items)
                except:
                    do_nothing = True    

                for to_insert in items_to_insert:    
                
                    validations_state = []

                    M_supplier_name = None
                    try:
                     M_supplier_name= to_insert['M_supplier_name']
                    except:
                        M_supplier_name = None

                    M_supplier_link = None
                    try:
                        M_supplier_link = to_insert['M_supplier_link']
                    except:
                        M_supplier_link = None

                    M_supplier_city = None 

                    try:
                        M_supplier_city = to_insert['M_supplier_city']
                    except:
                        M_supplier_city = None       



                    #Validate that each required field is available
                    validations_state.append(validate_field(field=M_supplier_name,is_number=False,message="M_supplier_name is Required"))
                    validations_state.append(validate_field(field=M_supplier_link ,is_number=False,message="M_supplier_link is Required"))
                    validations_state.append(validate_field(field=M_supplier_city ,is_number=False,message="M_supplier_city Required"))
                
                    failed_validations = []

                    #Validate that each required field is available

                    for validation in validations_state:
                        if validation != True:
                          
                            failed_validations.append(validation)

                    non_required_sql_fields = ''
                    non_required_sql_values = ''
                    
                    #Verify Data types if Non-mandatry fields have been provided
                    try:
                     if to_insert['O_supplier_state'] is not None:
                        Sup_State =  escape_val(to_insert['O_supplier_state'])
                        non_required_sql_fields = non_required_sql_fields + ","+ "Sup_State" + ','
                        non_required_sql_values = non_required_sql_values + ","+ Sup_State +','
                    except:
                        do_nothing = True
                    try:    
                     if to_insert['O_supplier_email'] is not None:
                        
                        Sup_email = escape_val(to_insert['O_supplier_email'])
                        non_required_sql_fields = non_required_sql_fields + "," + "Sup_email" + ','
                        non_required_sql_values = non_required_sql_values + "," + Sup_email+','
                  
                    except:
                        do_nothing = True
                    try:    
                     if to_insert['O_supplier_address'] is not None:
                      #try:
                       # int(to_insert['ID_Unit'])    
                        Sup_Address = escape_val(to_insert['O_supplier_address'])
                        non_required_sql_fields = non_required_sql_fields + "," + "Sup_Address" + ','
                        non_required_sql_values = non_required_sql_values + ","+Sup_Address+','
                      #except Exception as e:
                         
                       # failed_validations.append("ID_Unit must be a number")    
                    except:
                        do_nothing = True

                    try:
                     if to_insert['O_supplier_zip'] is not None:
                            Sup_ZIP = escape_val(to_insert['Sup_ZIP'])
                            non_required_sql_fields = non_required_sql_fields + "," + "Sup_ZIP"
                            non_required_sql_values = non_required_sql_values + "," + Sup_ZIP     
                    except:
                        do_nothing = True

                    try:
                     if to_insert['O_supplier_public'] is not None:
                            Sup_Public = escape_val(to_insert['O_supplier_public'])
                            non_required_sql_fields = non_required_sql_fields + "," + "Sup_Public"
                            non_required_sql_values = non_required_sql_values + "," + Sup_Public     
                    except:
                        do_nothing = True    
                   
                    if len(failed_validations) > 0:
                        failed_msg = ''
                        for fl in failed_validations:
                            failed_msg = failed_msg + str(fl) + ' , '
                        failed_inserts.append({"Supplier":to_insert,"Error":"Some errors were encountered - " + failed_msg})
                    else:
                        mandatory_sql_fields ='ID_Supp,Sup_Name,Sup_Link,Sup_City,'
                        mandatory_sql_values ='NULL,'+escape_val(M_supplier_name)+','+escape_val(M_supplier_link)+','+escape_val(M_supplier_city)+','                 
                        
                        constructed_sql = "INSERT INTO daudtco_BD_Prices.T_Supplier ("+mandatory_sql_fields+non_required_sql_fields+") VALUES ("+mandatory_sql_values+non_required_sql_values+");"
                        constructed_sql= constructed_sql.replace(',)',')')
                        constructed_sql= constructed_sql.replace(',,',',')
                      
                        insert_state = modify_record(constructed_sql)
                        insert_state_code = 400
                        if insert_state['status']==200:
                            if insert_state['rowsaffected'] == 1:
                             success_inserts.append({"Supplier":to_insert,"Success":"Inserted Successfully"})
                             insert_state_code = 200
                        else:
                            
                            failed_inserts.append({"Supplier":to_insert,"Error":"Failed to Insert - " + insert_state['message']})               
                
                return JsonResponse(status=insert_state_code,data={"success_inserted_count":len(success_inserts),"failed_inserts_count":len(failed_inserts),"success_items":success_inserts,"failed_items":failed_inserts})
     else:   
        return JsonResponse(status=401,data={"Error":"Permissions Denied"})    
    except:
        return JsonResponse(status=500,data={"Error":"Internal Server Error"})



@csrf_exempt
def add_product(request):
    items = request.POST.get('items')
    user = request.POST.get('user')
    key = request.POST.get('key')
  
    try:
     if  validate_api(user,key)['insert'] == True:
         if items is None:
                return  JsonResponse(status=400,data={"error":"Product(s) to insert required"})
         else:
            #Mandatory Fields
            #Description (name),Details (full details),SINAPI,subcategory (join to the T_SUB).Unit
                items_to_insert = []
                failed_inserts = []
                success_inserts = []
                try:
                    items_to_insert = ast.literal_eval(items)
                except:
                    do_nothing = True    

             
                for to_insert in items_to_insert:    
                
                    validations_state = []



                    M_ID_SUB = None
                    try:
                        M_ID_SUB = to_insert['M_Id_Sub']
                    except:
                        M_ID_SUB = None


                    M_Prod_Desc = None
                    try:
                         M_Prod_Desc = to_insert['M_Prod_name']
                    except:
                         M_Prod_Desc = None


                    M_Prod_Det = None
                    
                    try:
                         M_Prod_Det = to_insert['M_Prod_details']
                    except:
                        M_Prod_Det  = None


                    M_Prod_Sinapi =None
                    try:
                         M_Prod_Sinapi = to_insert['M_Prod_Sinapi']
                    except:
                        M_Prod_Sinapi  = None
                    

                    M_Prod_unit = None
                    try:
                         M_Prod_unit = to_insert['M_Prod_unit']
                    except:
                        M_Prod_unit  = None

                         

                    
                    
  

                    #Validate that each required field is available
                    validations_state.append(validate_field(field=M_ID_SUB,is_number=True,message="M_ID_SUB (Subactegory Id) is Required"))
                    validations_state.append(validate_field(field=M_Prod_unit ,is_number=True,message="M_Prod_unit (Unit Id) is Required"))
                    validations_state.append(validate_field(field=M_Prod_Desc ,is_number=False,message="M_Prod_Desc (Product Name) is Required"))
                    validations_state.append(validate_field(field=M_Prod_Det ,is_number=False,message="M_Prod_Det (Product Details) is Required"))
                    validations_state.append(validate_field(field=M_Prod_Sinapi ,is_number=False,message="M_Prod_Sinapi (Product Sinapi) is Required"))
                   
                    failed_validations = []

                    #Validate that each required field is available

                    for validation in validations_state:
                        if validation != True:
                          
                            failed_validations.append(validation)

                    non_required_sql_fields = ''
                    non_required_sql_values = ''
                    
                  
                  
                   
               
                    if len(failed_validations) > 0:
                        print(failed_validations)
                        failed_msg = ''
                        for fl in failed_validations:
                            failed_msg = failed_msg + str(fl) + ' , '
                        failed_inserts.append({"Products":to_insert,"Error":"Some errors were encountered - " + failed_msg})
                    else:
                        mandatory_sql_fields ='ID_Prod,Prod_Desc,Prod_Det,ID_SUB,Prod_Sinapi'
                        mandatory_sql_values ='NULL,'+escape_val(M_Prod_Desc)+','+escape_val(M_Prod_Det)+','+str(M_ID_SUB)+','+escape_val(M_Prod_Sinapi)                
                        
                        constructed_sql = "INSERT INTO daudtco_BD_Prices.T_Products ("+mandatory_sql_fields+") VALUES ("+mandatory_sql_values+");"
                        constructed_sql= constructed_sql.replace(',)',')')
                        constructed_sql= constructed_sql.replace(',,',',')


                        
                       
                        
                        insert_state = modify_record(constructed_sql)
                        insert_state_code = 400
                        if insert_state['status']==200:
                             if insert_state['rowsaffected'] == 1:
                              success_inserts.append({"Products":to_insert,"Success":"Inserted Successfully"})
                              insert_state_code = 200
                        else:
                               insert_state_code = 400
                               failed_inserts.append({"Products":to_insert,"Error":"Failed to Insert - " + insert_state['message']})               
                
                return JsonResponse(status=insert_state_code,data={"success_inserted_count":len(success_inserts),"failed_inserts_count":len(failed_inserts),"success_items":success_inserts,"failed_items":failed_inserts})

            

            
     else:
        
           return JsonResponse(status=401,data={"Error":"Permissions Denied"})     
    except:
        return JsonResponse(status=500,data={"Error":"Internal Server Error"})     


@csrf_exempt
def get_products_price(request):

    ID_Prod = request.GET.get('id')
    user = request.GET.get('user')
    key = request.GET.get('key')

    try:
     if  validate_api(user,key)['read'] == True:
         try:
            ID_Prod = int(ID_Prod)
         except:
             ID_Prod = None


         if ID_Prod is None:
                return  JsonResponse(status=400,data={"error":"Product Id to insert required"})
         else:
             #Check if the products exist
             SQL="select * from daudtco_BD_Prices.T_Products where ID_Prod={}".format(ID_Prod)
             products_found = fetch_records(SQL)

             if len(products_found) < 1:
                return  JsonResponse(status=400,data={"error":"Product with ID {} ".format(ID_Prod) + " does not exist"})
             else:
        

                   prod_name = products_found['records'][0][1]
                   sql_insert="select ID_Item, ID_Cor, Itm_Desc, Pric_Value, Unit_Name, max(Pric_Date) as Price, Cor_Factor, Bot_Link from daudtco_BD_Prices.T_correlations 	\
                      join (select T_Item_Bot.ID_Itm, Itm_Desc, Bot_Link from daudtco_BD_Prices.T_Item_Bot) Items	on T_correlations.ID_Item=Items.ID_Itm    \
                     join (select ID_Pric, Pric_Date, T_Prices.ID_Itm, Pric_value from daudtco_BD_Prices.T_Prices) Prices    on T_correlations.ID_Item=Prices.ID_Itm    \
                        join (select ID_Prod, Prod_unit, Unit_Name from daudtco_BD_Prices.T_Products     \
                         join (select ID_Unit, Unit_Name from daudtco_BD_Prices.T_Units) Units     on T_Products.Prod_unit=Units.ID_Unit    ) Products    on Products.ID_Prod=T_correlations.ID_Prod where T_correlations.ID_Prod={} group by ID_Item	".format(ID_Prod)
   
                   
                   avg=0.0
                   unit_name=""
                   row_len=0
                   min_price=0.0

                   products_price_found = fetch_records(sql_insert)

                   for row in products_price_found['records']:
                     #print("\033[31m ID Item: {} ID_Cor: {} \33[39m  Produto: \033[32m{}\33[39m Preço do produto: R$ {:0,.2f}   Preço por {}: R$ {:0,.2f} \n Registro de {} no Link: {}".format(row['ID_Item'],row['ID_Cor'],row['Itm_Desc'],row['Pric_value'],row['Unit_Name'],float(row['Pric_value'])/float(row['Cor_Factor']),row['Price'],row['Bot_Link']))
                     unit_price=float(row[3])/float(row[6])
                     avg+=unit_price
                     unit_name=row[4]
                     if unit_price<min_price and row_len>0:
                        min_price=unit_price
                     elif row_len==0:
                       min_price=unit_price
                       row_len+=1
         
                   if row_len>0:
                       return JsonResponse(status=200,data={"Prod_Id":str(ID_Prod),"Prod_Name":str(prod_name), "Avg_Price: R$":"{:0,.2f}".format(avg/row_len),"Unit":unit_name,"Min_Price: R$ ":"{:0,.2f}".format(min_price),"Unit":unit_name})
                        
                   else:
                       return  JsonResponse(status=400,data={"error":"No items found. Please create new correlations between Items and Products"})
                         

     else:
         return JsonResponse(status=401,data={"Error":"Permissions Denied"}) 
    except Exception as e:
        #print(e)
         return JsonResponse(status=500,data={"Error":"Internal Server Error"})              



   



def query(sql, cursor,connection):
    try:
     myc = connection.cursor()   
     myc.execute(sql)
     data = myc.fetchall()
     myc.close()
     return data
    except Exception as e:
       #print(e)
       return [{"error":str(e)}]  




def insert_db(sql, connection, cursor):
 
    rows_to_return = 0
    
    try:
     myc = connection.cursor()   
     myc.execute(sql)
     connection.commit()
     rows_to_return = myc.rowcount
     myc.close()
     return rows_to_return
    except Exception as e:
        #print(e)
        return 0 


def insert_many(sql, data, connection, cursor):
    
    rows_to_return = 0
    data_list = ast.literal_eval(data)
    #print(sql)
    #print(data)
    try:
     myc = connection.cursor()   
     myc.executemany(sql, data_list)
     connection.commit()
     rows_to_return =  myc.rowcount
     myc.close()
     return rows_to_return
    except Exception as e:
        #print(e)
        return 0


def getAvailableApiKeys(request):
    cursor = connection.cursor()


    result = query("SELECT * FROM daudtco_BD_Prices.T_apis",cursor,connection)
    

    data = [{"keys":[]},{"tables":[]}]
  
    all_tables = query("SHOW TABLES",cursor,connection)
    
    for table in all_tables:
       data[1]['tables'].append(str(table[0]))#'Tables_in_daudtco_BD_Prices'
        
    for res in result:
 
      data[0]['keys'].append({"api_user":str(res[1]),"api_key":str(res[2]),
             "data_created":str(res[3]),
             "canRead":str(res[4]),
             "canCreate":str(res[5]),
             "canUpdate":str(res[6]),
             "canDelete":str(res[7])})
    return JsonResponse({"data":data})








@csrf_exempt
def fetch_query(request):
    API = request.POST.get('API')
    API_QUERY = request.POST.get('sql_command')
    QUERY_TYPE = request.POST.get('command_type')
    QUERY_PAYLOAD = request.POST.get('sql_payload')
    

    
    


    if API is None:
         return JsonResponse(status=401,data={"command_type":"N/A","data":[],"errors":"Invalid API key Provided"})

    else:
        
        cursor = connection.cursor()
        escaped_key =str(API)
        sql = "SELECT * FROM daudtco_BD_Prices.T_Apis where api_key = '{}'".format(str(escaped_key))
    
        found_users =  query("SELECT * FROM daudtco_BD_Prices.T_Apis where api_key = '{}'".format(str(escaped_key)),cursor,connection)
 
        if len(found_users) == 1:
               
                    
                
            #Execute Query Here
                if QUERY_TYPE == 'get'  and API_QUERY is not None:
                
                   if int(found_users[0][4]) == 1: #'can_read' 
                    #User is requesting for a write operation
                     tosend =[]

                     try:

                         result = query(str(API_QUERY) ,cursor,connection)
                         tosend = result
                         return JsonResponse(status=200,data={"command_type":"get","data":tosend,"errors":None})
                     except Exception as e:
                         
                         return JsonResponse(status=500,data={"command_type":"N/A","data":[],"errors":"Some Error Occured.Message - "+ str(e)})
                     
                   else:  
                      return JsonResponse(status=400,data={"command_type":"N/A","data":[],"errors":"You do not have permissions to carry out a `get` command"})

                elif QUERY_TYPE == 'post_one' and API_QUERY is not None: 
                    if int(found_users[0][5]) == 1 or int(found_users[0][6]) == 1 or int(found_users[0][7]) == 1:      
                     #can_create,can_update,can_delete
                      try:
                         insertCount = insert_db(str(API_QUERY), connection, cursor)
                         if insertCount == 1:
                             return JsonResponse(status=200,data={"command_type":"post_once","data":[1],"errors":None})
                         else:
                             return JsonResponse(status=500,data={"command_type":"N/A","data":[0],"errors":"Failed to execute query.You might be having a mysql statement error"})    
                      except Exception as e:
                         
                         return JsonResponse(status=500,data={"command_type":"N/A","data":[],"errors":"Some Error Occured.Message - "+ str(e)})
                    
                    else:
                      return JsonResponse(status=400,data={"command_type":"N/A","data":[],"errors":"You do not have permissions to carry out a `update` or `delete' command"})
                elif QUERY_TYPE == 'post_many' and API_QUERY is not None :
                    if  QUERY_PAYLOAD is not None :   
                     #can_create,can_update,can_delete
                     if int(found_users[0][5]) == 1 or int(found_users[0][6]) == 1 or int(found_users[0][7]) == 1:      
                       
                        
                        try:
                         insertCount = insert_many(str(API_QUERY), QUERY_PAYLOAD , connection, cursor)
                         if insertCount > 0:
                             return JsonResponse(status=200,data={"command_type":"post_many","data":[str(insertCount)],"errors":None})
                         else:
                             return JsonResponse(status=500,data={"command_type":"N/A","data":[],"errors":"Failed to execute query. You might be having a mysql statement error"})    
                        except Exception as e: 
                         return JsonResponse(status=500,data={"command_type":"N/A","data":[],"errors":"Some Error Occured.Message - "+ str(e)})
                    
                       
                     else:
                        return JsonResponse(status=400,data={"command_type":"N/A","data":[],"errors":"You do not have permissions to carry out a `update` or `delete' command"})
                
                    else:    
                        return JsonResponse(status=400,data={"command_type":"N/A","data":[],"errors":" 'sql_payload' is required for bulk queries"})           
                else:
                     return JsonResponse(status=400,data={"command_type":"N/A","data":[],"errors":"Invalid `command_type` or `sql_command` value provided"})


            
           
        else:
            return JsonResponse(status=401,data={"command_type":"N/A","data":[],"errors":"Invalid API key Provided"})    
               



def getAPIpApiPage(request):
    if request.user.is_authenticated:
        return render(request,'index.html')
    else:
        return HttpResponse(status=401,content="Permissions Denied")    



#new API Methods
