$(document).ready(function(){
    
    //Populate Keys
    const tableData = $('table')[0]

    if(tableData !== undefined){
     let tableData = $('table')[0].innerHTML
     
     fetch('/get_keys/').then((res)=>{
         
      return res.json()
     }).then((data)=>{
         
       
         if(data.keys.length === 0){
            $('#fetchKeys').html('There are no API Keys currently Created')
             
         }else{
            //update modals to hava database Name
            let fetchedTables = data.tables 
            let sel_html = ' <option value="None">--Select Database(s) to  Give User Privildges To--</option>' 
            fetchedTables.forEach((table)=>{
              sel_html = sel_html + '<option value="'+table+'" >'+table+'</option>'
            })
            $('#api_db_new').html(sel_html)

            $('#fetchKeys').addClass('d-none')
             let to_append_html = ''

             data.keys.forEach((item)=>{
                 let canCreate = '<span class="text-danger fony-weight-bold">&cross;</span>'
                 let canRead = '<span class="text-danger fony-weight-bold">&cross;</span>'
                 let canUpdate = '<span class="text-danger fony-weight-bold">&cross;</span>'
                 let canDelete = '<span class="text-danger fony-weight-bold">&cross;</span>'
                 if (Number(item.can_insert) == 1){
                    canCreate = '<span class="text-success fony-weight-bold">&check;</span> '
                 }
                 if (Number(item.can_update) == 1){
                    canUpdate = '<span class="text-success fony-weight-bold">&check;</span> '
                 }
                 if (Number(item.can_delete) == 1){
                    canDelete = '<span class="text-success fony-weight-bold">&check;</span> '
                 }
                 if (Number(item.can_read) == 1){
                    canRead = '<span class="text-success fony-weight-bold">&check;</span> '
                 }
                 
                 to_append_html = to_append_html +   '<tr data-key_value="'+item.api_key+'">'+
                '<td>'+item.api_user+'</td>'+
                '<td><span class="keyvalue" style="background-color:whitesmoke" ><code>'+item.api_key+'</code></span></td>'+
                '<td>'+item.date_created+'</td>'+
                '<td>'+canRead+'</td>'+
                '<td> '+canCreate+'</td>'+
                '<td>'+canUpdate+'</td>'+
                '<td>'+canDelete+'</td>'+
                '<td class="delete_key"  style="cursor:pointer" title="Delete Key for '+item.api_user+'"> <i class="fa fa-trash text-danger"></i>  </td></tr>'


             })
             

             $('tbody')[0].innerHTML = to_append_html

               //delete Key
                document.querySelectorAll('.delete_key').forEach((key)=>{
                    key.addEventListener('click',function(){
                        let todelete  = this.parentNode.getAttribute('data-key_value')
                         
                        if (todelete !== undefined){
                            //detele
                            this.innerHTML = "Deleting <i class='fa fa-spin fa-spinner'></i>"
                            $.post('/delete_keys/','key='+todelete).then((data)=>{
                                this.innerHTML = "<small>Success Deleting</small>"
                                window.location.reload()
                            }).fail(()=>{
                                this.innerHTML = "<small>Failed to Delete</small>"

                                setTimeout(()=>{
                                    this.innerHTML = ' <i class="fa fa-trash text-danger"></i>  '
                                },3000)
                            })
                        }
                    })
                })


         }

    
     }).catch(()=>{
         console.log('error occured')
         $('#createKeyBTN').addClass('d-none')
         $('#fetchKeys').html('Something isn\'t okay ! Please refresh this page .If the problem persists, contact the admin')
     })

    }
    
    
    let APIFormnew = $('#newAPIKeys')



    //update database name on select
    const api_db_new_label = $('#api_db_new_label').html()
    $('#api_db_new').on('input',function(){
        
        $('#api_db_new_label').html("You selected <code>" +this.value + "</code> Database")
    })

    APIFormnew.submit(function(e){
        e.preventDefault()

        let formdata = $('#newAPIKeys').serialize()
        $('#newAPIKeys button').html('Creating API keys for ' + $('#apiuser_new').val() + ' .Please wait <i class="fa fa-spin fa-spinner"></i>')
        $.post('/generate_key/',formdata,(data)=>{
       
            $('.modal-body')[0].innerHTML = "<div class='alert alert-success '>Success ! API Key for <span class='font-weight-bold'>"+$('#apiuser_new').val()+"</span> has been created succesfuly. Key for User is  `<code>"+data.key+"</code>`</div>"
            $('#fetchKeys').removeClass('d-none').html("Some new API keys have been created. Please refresh this page to view the newly added keys")
        }).fail((xhr)=>{
            $('#newAPIKeys button').html("Failed." + xhr.responseText)
        })
      
    })


  
})