from django.http import JsonResponse
from django.shortcuts import render
from django.db import connections
from django.db.utils import ConnectionDoesNotExist,OperationalError,IntegrityError,DatabaseError,ProgrammingError,DataError,NotSupportedError
import secrets
from django.core.exceptions import ValidationError
from django.core.validators import validate_email,validate_comma_separated_integer_list,validate_slug,URLValidator,RegexValidator
from password_validator import PasswordValidator
from django.contrib.auth.models import User
import re
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
import time,datetime



def query(sql):
   try: 
    cursor = connections['daudt_product_related'].cursor()
    cursor.execute(sql)
    #Query performed
    #print(connection.queries[-1])
    return cursor.fetchall()
   except Exception as e:
        return None



def insert_query(sql):
    try:
        cursor = connections['daudt_product_related'].cursor()
        cursor.execute(sql)
        connections.commit()
        return cursor.lastrowid
    except Exception as e:
  
        return None         


@login_required
def get_dashboard(request):
    return render(request,'new_dashboard.html')


@login_required
def get_available_keys(request):
    #method has to be GET
    if request.method != 'GET':
        return JsonResponse(status=400,data={'error':'Bad Request'})

    #Get all available keys
    try:
       cursor = connections['apis'].cursor()

       cursor.execute("select  key_value, key_user, date_created, can_select, can_insert, can_update, can_delete, is_super from api_keys")
       
       columns = [col[0] for col in cursor.description]
       rows = []
       for row in cursor.fetchall():
               rows.append(dict(zip(columns, row)))
              

       return JsonResponse(status=200,data={'users':rows})

    except Exception as e:
       
        return JsonResponse(status=400,data={'error':'Bad Request'})




@login_required
def api_key_creation(request):
  try:
    #Method has to be POST
    if request.method != 'POST':
        return JsonResponse(status=400,data={'error':'Bad Request'})

    to_create_user = request.POST.get('api_user')
    if to_create_user is None:
        return JsonResponse(status=400,data={'error':'Api User is Required'})

    #check if username contains only letters
    if not to_create_user.isalpha() or len(str(to_create_user)) < 3:
        return JsonResponse(status=400,data={'error':'Api User must contain only letters and be atleast 3 characters long'})    

    #check if that username already exists in the database
    try:
        cursor = connections['mysql'].cursor()
        cursor.execute("select * from  user  where User = '{}'".format(to_create_user))
        if cursor.fetchone() is not None:
            return JsonResponse(status=400,data={'error':'User already exists'})        
    except:
        return JsonResponse(status=500,data={'error':'Internal Server Error'})        


    #Generate the password
    generated_key = secrets.token_urlsafe(10)  
 


        #verify that all input fields are provided
    if request.POST.get('priviledge_read') is None or request.POST.get('priviledge_all') is None or request.POST.get('priviledge_insert') is None or request.POST.get('priviledge_update') is None or request.POST.get('priviledge_delete') is None:
        return JsonResponse(status=400,data={'error':'Missing Fields'})

    #Create the user 
    # 
    sql_new_user = "create user '{}'@'%' identified by '{}';".format(to_create_user,generated_key)
    try:
        cursor = connections['mysql'].cursor()
        cursor.execute(sql_new_user)
        connections['mysql'].commit() 
    except Exception as e:
        return JsonResponse(status=500,data={'error':'Internal Server Error'})
    
    can_select = 'N'
    can_insert = 'N'
    can_update = 'N'
    can_delete = 'N'
    is_super = 'N'


    if request.POST.get('priviledge_all') == 'false':


        if request.POST.get('priviledge_read') == 'true':
            can_select = 'Y'
            sql_read = "grant Select  on * . * TO '{}'@'%';".format(to_create_user)
            try:
                cursor = connections['mysql'].cursor()
                cursor.execute(sql_read)
                connections['mysql'].commit()
                
            except:
                
                return JsonResponse(status=500,data={'error':'Internal server error'})


        if request.POST.get('priviledge_insert') == 'true':
            can_insert = 'Y'
            sql_insert = "grant INSERT  on * . * TO '{}'@'%';".format(to_create_user)

            try:
                cursor = connections['mysql'].cursor()
                cursor.execute(sql_insert)
                connections['mysql'].commit()
          
            except:
                return JsonResponse(status=500,data={'error':'Internal server error'})

        if request.POST.get('priviledge_update') == 'true':
            can_update = 'Y'
            sql_update = "grant Update  on * . * TO '{}'@'%';".format(to_create_user)
            try:
                cursor = connections['mysql'].cursor()
                cursor.execute(sql_update)
                connections['mysql'].commit()
           
            except:
                return JsonResponse(status=500,data={'error':'Internal server error'})

        if request.POST.get('priviledge_delete') == 'true':
            can_delete = 'Y'
            sql_delete = "grant Delete  on * . * TO '{}'@'%';".format(to_create_user)
            
            try:
                cursor = connections['mysql'].cursor()
                cursor.execute(sql_delete)
                connections['mysql'].commit()

            except:
                return JsonResponse(status=500,data={'error':'Internal server error'})

   
    #user is super user
    if request.POST.get('priviledge_all') == 'true':
        sql_all = "grant ALL PRIVILEGES  on * . * TO '{}'@'%';".format(to_create_user) 
        try:
            cursor = connections['mysql'].cursor()
            cursor.execute(sql_all)
            connections['mysql'].commit()

        except:
           return JsonResponse(status=500,data={'error':'Internal server error'})    
    #Insert that user to api_keys table
    sql ="insert into  api_keys (key_value, key_user, can_select, can_insert, can_update, can_delete, is_super) values ('{}','{}','{}','{}','{}','{}','{}') ".format(generated_key,to_create_user,can_select,can_insert,can_update,can_delete,is_super)        
    
    cursor = connections['apis'].cursor()
    cursor.execute(sql)
    connections['apis'].commit()
    insert_count = cursor.rowcount
    if insert_count != 1:
       print("failed to insert") 
       return JsonResponse(status=500,data={'error':'Internal server error'})
    #We have given all permisions to user
    return JsonResponse(status=200,data={'user':to_create_user,'key':generated_key})        

  except Exception as e:
        return JsonResponse(status=500,data={'error':'Internal Server Error'})

@login_required    
def delete_api_key(request):
    if request.method != 'POST' :
        return JsonResponse(status=400,data={'error':'Method not allowed'})
    
    key_to_delete = request.POST.get('key')
    if key_to_delete is None:

        return JsonResponse(status=400,data={'error':'Key to delete is required'})
    #
    sql = "delete from api_keys where key_user = '{}'".format(key_to_delete)
    try:

         cursor = connections['apis'].cursor()
         cursor.execute(sql)
         connections['apis'].commit()    
         deleted_count = cursor.rowcount
    

         if deleted_count != 1:
            return JsonResponse(status=400,data={'error':'Internal server error'})
         
         sql2 = "DROP USER '{}'@'%';".format(key_to_delete)
         try:
            cursor2 = connections['mysql'].cursor()
            cursor2.execute(sql2)
            connections['mysql'].commit()    
         except:
            return JsonResponse(status=400,data={'error':'Internal server error'})
         return JsonResponse(status=200,data={'message':'Successfully deleted'})  

    except:
        return JsonResponse(status=500,data={'error':'Internal Server Error'})




@csrf_exempt
def register(request):
  try:  
   if request.method == 'POST':
    validation_errors = []

    email = request.POST.get('new-user-email')
    password = request.POST.get('new-user-password')
    fullname = request.POST.get('new-user-full-name')
    company_name = request.POST.get('new-user-company-name')



    

    try:
        validate_email(email)
    except ValidationError:
        validation_errors.append('Enter a valid email adress')
 
    schema = PasswordValidator()
    schema.min(6).has().letters().has().digits()  #.has().symbols()
    
    if schema.validate(password) is False:
        validation_errors.append('Password must be at least 6 characters long, contain letters and digits')
    try:
        rgx = RegexValidator(regex=r'^[a-zA-Z ]+$', message="Fullname must contain only letters and spaces")
        rgx(fullname)
    except ValidationError:
        validation_errors.append('Fullname must contain only letters and spaces')    
    

    try:
        rgx = RegexValidator(regex=r'^[a-zA-Z ]+$', message="Company name  must contain only letters and spaces")
        rgx(fullname)
    except ValidationError:
        validation_errors.append('Company name must contain only letters and spaces')  

    if len(validation_errors) > 0:
        errors_to_html = '<div class="pb-1" style="text-decoration:underline">Some errors were encountered : </div> '
        for error in validation_errors:
            errors_to_html += '<span style="font-size:small" >- ' + error + '</span><br>' 
        return JsonResponse(status=400,data={'errors':errors_to_html})
    else:
        
        #Check if user exists in database already
        try:
            user = User.objects.get(email=email)
            return JsonResponse(status=400,data={'errors':'The email address you provided is already associated with another account'})
        except User.DoesNotExist:
            #Add user to Database 
            firstName = fullname.split(' ')[0]
            lastName = ' '.join(fullname.split(' ')[1:])
            user_name = str(email.split('@')[0])

            save_state = False
            try:
             new_user = User.objects.create_user(username=user_name, email=email,password=password,first_name=company_name,last_name=company_name,is_staff=False,is_active=False)
             new_user.save()

             save_state = True
            except Exception as e:
            
                save_state = False
            if save_state is False:
                return JsonResponse(status=500,data={'errors':'User could not be created'})
            else:
                #Generate email confirmation token
                get_current_milliseconds = str(int(time.time() * 1000))
                token = 'signup-'+str(secrets.token_urlsafe(32)) + str(get_current_milliseconds)
               

                

                #Send token to database
                token_expires = datetime.date.today() + datetime.timedelta(days=3000)
                
                sql = "INSERT INTO daudtco_BD_Prices.T_accounts_recovery (TokenValue,UserEmail,TokenExpiry) values ('{}','{}','{}')".format(str(token),email,str(token_expires))
              
                inserted_token = insert_query(sql)
                if inserted_token is not None:
                    email_sent = emails.token_confirm_email(str(token),email,'signup')
                    if email_sent:
                        #add that user to database
                        sql = " insert into daudtco_BD_Prices.T_user (US_email, US_name, US_pass, US_social, US_picture, ID_Sup, US_isadmin) "\
                            " values ('{}','{}','{}','{}','{}','{}','{}') ;".format(
                             email,company_name,password,'Manual','Null',-1,0)

                        new_user_inserted = insert_query(sql)
                        if new_user_inserted is not None and new_user_inserted>0:                     
                           return JsonResponse(status=200,data={'message':"sucsess"})

                       
                        return JsonResponse(status=200,data={'message':"sucsess"})   

                    else:
                        return JsonResponse(status=500,data={'errors':'Failed to send Confirmation Email'}) 
                else:
                    #Delete user from database
                    try:
                     u = User.objects.get(username = user_name)
                     u.delete()
                     return JsonResponse(status=500,data={'errors':'Failed to create confirmation token'})         

                    except User.DoesNotExist:
                       return JsonResponse(status=500,data={'errors':'Failed to register user - non-existence'})  

                    except Exception as e: 
                       return JsonResponse(status=500,data={'errors':'Failed to register user'})  
                    
              

        except Exception as e:
         
            return JsonResponse(status=500,data={'errors':'Something went wrong'})
   else:
    return JsonResponse(status=400,data={'errors':'Invalid request'})
  except:
      return JsonResponse(status=500,data={'errors':'Something went wrong'})

@csrf_exempt
def login_user(request):
    try:
        if request.method == 'POST':
            email = request.POST.get('email')
            password = request.POST.get('password')
      
            try:
                validate_email(email)
            except ValidationError:
                return JsonResponse(status=400,data={'errors':'Enter a valid email adress'})
            

            try:
                user_name =str(email.split('@')[0])
                
                try:
                  check_user_verification = User.objects.get(username=user_name)
                  if check_user_verification is not None and check_user_verification.is_active is False:
                     return JsonResponse(status=403,data={'errors':'<p style="font-size:small">Your account is inactive. <br><br>If you recently created this account,please ensure you verified your email by clicking on link sent to your email.<br>Otherwise ,contact <span style="text-decoration:underline;color:black">support@painelcnstru.com.br</span> for assistance </span>'})
                  else:

                      user = authenticate(username=user_name, password=password)
                      if user is not None:
                        login(request, user)
                        return JsonResponse(status=200,data={'message':'success'})
                      
                      else:
                        return JsonResponse(status=400,data={'errors':'Wrong username and Password Combination'})

                except User.DoesNotExist:
                 return JsonResponse(status=400,data={'errors':'Wrong username and Password Combination'})

                 
               
                
                
              
            except Exception as e:
                return JsonResponse(status=500,data={'errors':'Something went wrong'})
        else:
         return JsonResponse(status=400,data={'errors':'Invalid request'})
    except:
        return JsonResponse(status=500,data={'errors':'Something went wrong'})
    
def activate_email(request):
    logout(request)
    
    activate_token = request.GET.get('token')

    if activate_token is None:
        return render(request,'generic.html',{"error":'The token you provided is invalid.Please check your email address for confirmation link.'})
    if str(activate_token).find('reset-') > -1:
              return render(request,'generic.html',{"error":'The token you provided is invalid.Please check your email address for confirmation link.'})
  

    else:
        sql = "Select UserEmail from daudtco_BD_Prices.T_accounts_recovery where TokenValue = '{}' ".format(str(activate_token))

        found_tokens = query(sql)

        if len(found_tokens) == 1:
            #Token is valid,validate it by creating new supplier with that email if they do not exist

                #That supplier already exists,marki email as veriied in djang admin
                  try:
                        user_itm = User.objects.get(email=found_tokens[0][0])
                        user_itm.is_active = True
                        user_itm.save()
                        html = '<i class="fa fa-check-square"></i> Your email has been verified and your account is now fully activated. <a style="text-decoration:none" href="/authenticate/">Click here to login</a>'
                        return render(request,'generic.html',{"message":html})
                  except User.DoesNotExist:
                      html = 'Email verification Failed. Your verification token is either expired or broken. <a href="/authenticate">Click here to login or request a new token</a>'
                      return render(request,'generic.html',{"error":html})
                      #return HttpResponseBadRequest('<h1 style="margin-left:1%;margin-top:1%">Something isn\'t right on our end.Please contact support to resolve this issue at <span style="font-weight:bold;text-decoration:underline">support@painelconstru.com.br</span> </h1>')     
                 

        else:
               return render(request,'generic.html',{"error":'The token you provided is invalid.Please check your email address for confirmation link. '})


def validate_password_reset_token(request):
    token = request.GET.get('token')

    if token is None:
         return render(request, 'authenticate.html',{"resetForm":True,"authentication":False,"tokenValid":False}) 

    #check if token contains signup and reject
    if token.find('signup-') > -1:
             return render(request, 'authenticate.html',{"resetForm":True,"authentication":False,"tokenValid":False}) 

    #Check token n database, if valid ,render password reset form
    sql = "Select UserEmail from daudtco_BD_Prices.T_accounts_recovery where TokenValue = '{}' ".format(str(token))

    found_tokens = query(sql)

    if len(found_tokens) != 1:
        return render(request, 'authenticate.html',{"resetForm":True,"authentication":False,"tokenValid":False})  
    
    #Token is valid,validate it by creating new supplier with that email if they do not exist
    return render(request, 'authenticate.html',{"resetForm":True,"authentication":False,"tokenValid":True,"email":found_tokens[0][0]})      

@csrf_exempt
def recover_account(request):
    #get the email,check
    user_email  = request.POST.get('email')

    try:
        validate_email(user_email)
    except:
        user_email = None        
    
    if user_email is None:
        return JsonResponse(status=400,data={"errors":"Email is required"})

    

    #Genmerate email confirmation token
    get_current_milliseconds = str(int(time.time() * 1000))
    token = 'recover-'+str(secrets.token_urlsafe(32)) + str(get_current_milliseconds)
    #Send token to database
    token_expires = datetime.date.today() + datetime.timedelta(days=1)
                
    sql = "INSERT INTO daudtco_BD_Prices.T_accounts_recovery (TokenValue,UserEmail,TokenExpiry) values ('{}','{}','{}')".format(str(token),user_email,str(token_expires))
              
    inserted_token = insert_query(sql)
    if inserted_token is  None:
        return JsonResponse(status=500,data={'errors':'Internal Server Error'}) 
    email_sent = emails.token_confirm_email(str(token),user_email,'reset')
    if email_sent:            
                return JsonResponse(status=200,data={'message':"sucsess"})
    else:
                return JsonResponse(status=500,data={'errors':'Failed to send Confirmation Email'}) 


@csrf_exempt
def reset_account(request):
    action_type = request.POST.get('action')
    new_password = request.POST.get('new_password')
    old_password = request.POST.get('old_password')
    user_email = request.POST.get('username')
    schema = PasswordValidator()
    schema.min(6).has().letters().has().digits()  #.has().symbols()
    if action_type == 'update-password':
       if request.user.is_authenticated:   
            #Check current password,if it mmatches user password,update it
            user = authenticate(username=request.user.get_username(), password=str(old_password))
            if user is not None:
              # A backend authenticated the credentials
              validation_errors =[]

              if schema.validate(new_password) is False:
                validation_errors.append('A senha deve ter pelo menos 6 caracteres, conter letras e dígitos')
                

              if len(validation_errors) > 0:
                return JsonResponse(status=400,data={"error":validation_errors[0]})
              else:
                  user.set_password(str(new_password))
                  user.save()
                  return JsonResponse(status="200",data={"message":"Password succesfully Updated"})


            else:
             # No backend authenticated the credentials
                return JsonResponse(status=400,data={"error":"A senha atual que você forneceu está incorreta"})  

       else:
           return JsonResponse(status=403,data={"error":"Request Forbidden"})    
    
    else:
      #Just update the password
      if new_password is None:
          return JsonResponse(status=400,data={"errors":"Password is required"})
      else:
          validation_errors =[]
          #Check password strength and if okay,update 

    
          if schema.validate(new_password) is False:
            validation_errors.append('Password must be at least 6 characters long, contain letters and digits')
          

          if len(validation_errors) > 0:
              return JsonResponse(status=400,data={"errors":validation_errors[0]})
          else:
             user_name =str(user_email.split('@')[0]) 
             
             try:
                
                
                logged_user = User.objects.get(username=user_name)
                logged_user.set_password(str(new_password))
                logged_user.save()
                return JsonResponse(status="200",data={"message":"Password succesfully Updated"})
             except Exception as e:
                print(e)
                return JsonResponse(status=500,data={'errors':'Internal server error'})
              

