from django.urls import path
import accounts.views as view

urlpatterns = [
    path('get/users/',view.get_available_keys),
    path('dashboard/',view.get_dashboard),
    path('create/user/',view.api_key_creation),
    path('delete/user/',view.delete_api_key)
]
