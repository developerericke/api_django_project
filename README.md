# README #

Get Started with Daudt API by the setup guide below


## Endpoints ##

### 1. GET Endpoints ###

#### - Fetching All Categories #####

URL - https://api.painelconstru.com.br/categories/?user=api_user&key=api_key

***N/B for fetching a specific Category, supply ```id``` param***

***Required Params***

-user (api user)

-key  (api key)

-id (Optional - to be used where only a specific category is being fetched)


***Output Fomart (JSON)***

```
{"VAL": 
  [ 
   {"ID": 28,
   "Cat": "Arquitetura",
   "SubCat": [
          {"ID": 2, "SCat": "Pisos", "Sub_Uuid": null, "Sub_Link": null, "Sub_Corel": 234},
          {"ID": 3, "SCat": "Revestimentos", "Sub_Uuid": null, "Sub_Link": null, "Sub_Corel": 235}, 
          {"ID": 41, "SCat": "Outros Arquitetura", "Sub_Uuid": null, "Sub_Link": null, "Sub_Corel": 1}
          ]
   }
   ]
}   
 
```
 

 
#### - Fetching All Units ####

URL - https://api.painelconstru.com.br/units/?user=api_user&key=api_key

***N/B for fetching a specific Unit, supply the ```id``` param***

***Required Params***

-user (api user)

-key  (api key)

-id (Optional - to be used where only a specific unit is being fetched)

***Sample Output Fomart (JSON)***

```
{"VAL1":
  [
    {"ID": 1, "Unit": "Unid."},
    {"ID": 2, "Unit": "metro"},
    {"ID": 3, "Unit": "m\u00b2"}, 
    {"ID": 4, "Unit": "m\u00b3"}, 
    {"ID": 5, "Unit": "kg"}, 
    {"ID": 6, "Unit": "conjunto"}
  ]
}
  
 
```

#### - Fetching All Suppliers ####

URL - https://api.painelconstru.com.br/suppliers/?user=api_user&key=api_key

***N/B for fetching a specific supplier, supply the ```id``` param***

***Required Params***

-user (api user)

-key  (api key)

-id (Optional - to be used where only a specific supplier is being fetched)

***Sample Output Fomart (JSON)***

```
{"VAL1":
  [
    {"ID": 4, "Name": "Siemens","www.siemens.com.br":}
  ]
}
  
 
```

#### - Fetching All Subcategories ####

URL - https://api.painelconstru.com.br/subcategories/?user=api_user&key=api_key

***N/B for fetching a specific subcategory, supply the ```id``` param***

***Required Params***

-user (api user)

-key  (api key)

-id (Optional - to be used where only a specific subcategory is being fetched)

***Sample Output Fomart (JSON)***

```
{"Subcategories":
  [
    {"ID": 1, "Name": "Sem Categoria"},

  ]
}
  
 
```
 
#### - Fetching Product Price ####

URL - https://api.painelconstru.com.br/products/get/price/?user=api_user&key=api_key&id=product_id



***Required Params***

-user (api user)

-key  (api key)

-id (Id of product we want to fetch the price)

***Sample Output Fomart (JSON)***

```
{
  "Prod_Id":5518,
  "Prod_Name":"VALVULA EM METAL CROMADO PARA PIA AMERICANA 3.1/2 X 1.1/2 \"",
  "Avg_Price: R$":"72.22",
  "Unit": "Unid.",
  "Min_Price: R$ ":"72.22"

}
  
 
```
 
### 2. POST Endpoints ####

**N/B All keys/fiels with a prefix of M indicate Mandatory field whereas all Fields with prefix O indicate Optional fields**

All POST requests will have the same response fomart (JSON) and return status code 200.Otherwise, different JSON will be returned
indicating the error if any

For status Code 200, JSON fomart of the response will be as below

```
// It is upto the developer to check the status code of the response before trying to access response JSON keys
//Despite the response code being 200,it does not entirly imply that all records were posted to database succesully


{
"success_inserted_count": 0, //indicates total number of items that were inserted succesfully 
"failed_inserts_count": 0, //indicates total number of items that failed to be inserted due to some reason i.e value error
"success_items": [], //Return all items that were inserted succesfully
"failed_items": []  //Return all items that failed to be inserted
}


```


#### - Adding New Item(s) ####

URL - https://api.painelconstru.com.br/items/add
 
**Required Fields**

-user

-key

-items (A stringified array containing the item/items to be inserted)


**Data fomart of the records to Insert**

``` 
    [
       {
          'M_supplier_id':4, (int)
          'M_item_status':1, (int)
          'M_bot_link':'eric.test.com', (String)
          'M_item_desc':'test eric', (String)
          'M_item_det':'lorem ipsum test eric', (String)
          'M_item_user':0,(int)    
          'O_item_code':0, (int) //Optional,may be excluded as part of formdata ,Remember,prefix O_ means the field can be excluded
          'O_id_man':0,(int)    //Optional,may be excluded as part of formdata ,Remember,prefix O_ means the field can be excluded
          'O_id_unit':0, (int)   //Optional,may be excluded as part of formdata ,Remember,prefix O_ means the field can be excluded
          'O_id_sub':0 (int)    //Optional,may be excluded as part of formdata ,Remember,prefix O_ means the field can be excluded
      }
   ]
```

#### -Adding Price(s) ####

URL - https://api.painelconstru.com.br/prices/add

**Required Fields**

-user

-key

-items (A stringified array containing the item/items to be inserted)
 
 
**Data fomart of the records to Insert**

```
 
[ {
             "M_item_id":100000, (Int)
             "M_price_value":55, (Int)
             "M_price_user":15, (Int)
              "O_price_date":'2021-08-25 00:00:00', (String)
             "O_price_due_date":'2021-09-02 00:00:00', (String)
             "O_price_obsolete":0 (int)
        }
        
]
```

#### - Adding Image(s) ####

URL - https://api.painelconstru.com.br/images/add

**Required Fields**

-user

-key

-items (A stringified array containing the item/items to be inserted)
 
 
**Data fomart of the records to Insert**

```
 
[
     {
             "M_item_id":100000, (int)
             "M_image_link":"https://image.shutterstock.com/image-photo/sr-600w-1227812149.jpg" (string)
 
       }
]
```

#### - Adding Manufacturers(s) ####

URL - https://api.painelconstru.com.br/manufacturers/add

**Required Fields**

-user

-key

-items (A stringified array containing the item/items to be inserted)
 

**Data fomart of the records to Insert**

```
 
[ 
    {
             "M_manufacturer_name":"ERIC_MAN", (string)
             "M_manufacturer_link":"test_link" (string)
      }
]
```

#### -Adding Supplier(s) ####

URL - https://api.painelconstru.com.br/supplier/add

**Required Fields**

-user

-key

-items (A stringified array containing the item/items to be inserted)
 
 
**Data fomart of the records to Insert**

```
[     
      {
             "M_supplier_name":"Supplier Eric", (string)
             "M_supplier_link":"eric.com", (string)
             "M_supplier_city":"Nairobi" (string)
        }
        
]
```


#### -Adding Product(s) ####

URL - https://api.painelconstru.com.br/products/add

**Required Fields**

-user

-key

-items (A stringified array containing the item/items to be inserted)
 
 
**Data fomart of the records to Insert**

```
[     
      {
             "M_Id_Sub":76, (int)
             "M_Prod_unit":5, (int)
             "M_Prod_name":"Name of product" (string),
             "M_Prod_details":"Description of product" (string),
             "M_Prod_Sinapi","Product Sinapi" (string),
        }
        
]
```

#### -Performing Raw Sql - FETCHING DATA ####

URL - https://api.painelconstru.com.br/sql/api/read/

**Required Fields**

-user

-key

-sql (sql command to run)
 
 
**Sample code in python**

```
  import requests


  def test_api():
      url = 'https://api.painelconstru.com.br/sql/api/read/' 
      sql = "select * from T_Item_Bot limit 5"
      data = {"sql":sql,"key":"your_key_here","user":"your_user_here"}

      req = requests.post(url,data=data)

      print(req.text)
      //returns a data object which is an array of output as shown below

      {"data": [
        {"ID_Itm": 25, "Itm_Code": "770590", "ID_Sup": 42, "Itm_Status": 1, "ID_Man": null, "Bot_Link": "https://www.cassol.com.br/conector-macho-tigre-25mmx3-4--ppr-verde/p", "Itm_Desc": "Conector Macho Tigre 25mmx3/4\" PPR Verde", "Itm_Det": "Conector Macho Tigre 25mmx3/4\" PPR Verde", "Itm_user": 4, "ID_Unit": 1, "ID_Sub": 1, "Itm_man_code": null, "is_private": 0, "affiliate_link": null}
            ]
      }

      //It is advisable that you rely on the query status code to confirm whether a query resolved succesfully or not.
      i.e if the status code is 200 then the query was successful.
      Any other status code other than 200 implies that the query failed.A message will be sent to show
      the reason for failure.


  test_api()  //call the function
```


#### -Performing Raw Sql - WRITING/UPDATING RECORDS ####

URL - https://api.painelconstru.com.br/sql/api/write/

**Required Fields**

-user

-key

-sql (sql command to run)
 
 
**Sample code in python**

```
  import requests


  def test_api():
      url = 'https://api.painelconstru.com.br/sql/api/write/' 
      sql = "update T_Products set Prod_Tags = NULL where ID_Prod = 5807"
      data = {"sql":sql,"key":"your_key_here","user":"your_user_here"}
      
      req = requests.post(url,data=data)

      print(req.text)
      //output {"affected_rows": 1, "last_id": 25}

      //The json response shows affected rows count and last id incase its an insert query

  

    
      //It is advisable that you rely on the query status code to confirm whether a query resolved succesfully or not.
      i.e if the status code is 200 then the query was successful.
      Any other status code other than 200 implies that the query failed.A message will be sent to show
      the reason for failure.


  test_api()  //call the function
```
#### - Note on performing raw sql ####
  When performing raw sql, please ensure you use the correct urls since the response will be different.For instance ,queries that update records will return a json object with the affected rows count and last id while queries that fetch data will return a json object with the data.

  You will be able to perform raw sql queries on databases you have access to and can your privildges i,e ```CREATE, DELETE ,UPDATE, INSERT ,SELECT ``` will be decided by the administrator. Incase you have issues and belive you should be able to perform a query, please contact admin.

  All  errors will be resturned with a status code of non-200 and a message will be sent to show the reason for failure.