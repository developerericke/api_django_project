from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
import json
from django.db import connections
from django.db.utils import ConnectionDoesNotExist,OperationalError,IntegrityError,DatabaseError,ProgrammingError,DataError,NotSupportedError
import mysql.connector 




def validate_user(**kwargs):
    #Create a connections to the database
   try:
    cnx = mysql.connector.connect(
        user=kwargs['user'],
        password = kwargs['pass_db'],
        database=kwargs['db'],
        port=3306,
        host='178.79.159.165',
    )
    return {"connection":cnx,"error":None}
   except Exception as e:
      return {"connection":None,"error":e}
        
    
@csrf_exempt
def sql_query(request):
    if request.method != 'POST':
        return JsonResponse(status=400,data={'error':'Bad Request'})

    key = request.POST.get('key')
    sql = request.POST.get('sql')
    api_user = request.POST.get('user')
  

    if key is None or sql is None or api_user is None:
        return JsonResponse(status=400,data={'error':'Api User,key and sql statement are Required'})

    db_connection = validate_user(user=api_user,pass_db=key,db='daudtco_BD_Prices')   

    if db_connection["error"]  is not None:
        return JsonResponse(status=400,data={'error':str(db_connection["error"])})
    
    #credentials were succesfull
    try:
        cursor = db_connection['connection'].cursor()
        cursor.execute(sql)
        columns = [col[0] for col in cursor.description]
        rows = []
        for row in cursor.fetchall():
            rows.append(dict(zip(columns, row)))
        db_connection['connection'].close()    
        return JsonResponse(status=200,data={'data':rows})
    except Exception as e:
        return JsonResponse(status=400,data={'error':str(e)})   

@csrf_exempt
def sql_query(request):
    if request.method != 'POST':
        return JsonResponse(status=400,data={'error':'Bad Request'})

    key = request.POST.get('key')
    sql = request.POST.get('sql')
    api_user = request.POST.get('user')
  

    if key is None or sql is None or api_user is None:
        return JsonResponse(status=400,data={'error':'Api User,key and sql statement are Required'})

    db_connection = validate_user(user=api_user,pass_db=key,db='daudtco_BD_Prices')   

    if db_connection["error"]  is not None:
        return JsonResponse(status=400,data={'error':str(db_connection["error"])})
    
    #credentials were succesfull
    try:
        cursor = db_connection['connection'].cursor()
        cursor.execute(sql)
        columns = [col[0] for col in cursor.description]
        rows = []
        for row in cursor.fetchall():
            rows.append(dict(zip(columns, row)))
        db_connection['connection'].close()    
        return JsonResponse(status=200,data={'data':rows})
    except Exception as e:
        return JsonResponse(status=400,data={'error':str(e)})          



@csrf_exempt
def sql_write(request):
    if request.method != 'POST':
        return JsonResponse(status=400,data={'error':'Bad Request'})

    key = request.POST.get('key')
    sql = request.POST.get('sql')
    api_user = request.POST.get('user')
  

    if key is None or sql is None or api_user is None:
        return JsonResponse(status=400,data={'error':'Api User,key and sql statement are Required'})

    db_connection = validate_user(user=api_user,pass_db=key,db='daudtco_BD_Prices')   

    if db_connection["error"]  is not None:
        return JsonResponse(status=400,data={'error':str(db_connection["error"])})
    
    #credentials were succesfull
    try:
        cursor = db_connection['connection'].cursor()
        cursor.execute(sql)
        db_connection['connection'].commit()
        affected_rows = cursor.rowcount
        last_id = cursor.lastrowid
        db_connection['connection'].close()    
        return JsonResponse(status=200,data={'affected_rows':affected_rows,'last_id':last_id})
    except Exception as e:
        return JsonResponse(status=400,data={'error':str(e)})    