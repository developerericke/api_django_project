from django.urls import path


from . import views

urlpatterns = [
    path('read/',views.sql_query),
    path('write/',views.sql_write)
]
